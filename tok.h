/*
 * tok.h
 *
 *   Created on: Jan 21, 2021
 *       Author: dan
 *  Description: Library for a mixture of escape sequence decoding as in C and tokenizing as in bash
 *
 */

#ifndef TOK_H_
#define TOK_H_

#include "tokbase.h"

#ifdef __cplusplus
extern "C" {
#endif

void print_with_null(char str[], SIZE_T strsize, FILE *stream);
void remove_char(char str[]);
void insert_char(char str[], char c);
bool needs_quoting(const char str[]);
char escape_to_char(char c);
char char_to_escape(char c);
int  tokenize(char str[], SIZE_T *tokenc, char *tokenv[], SIZE_T max_tokens);
void escape_string(char str[], const char do_not_escape[]);
void escape_string_auto(char str[]);
void assemble_from_tokens(char str[], SIZE_T tokenc, const char *tokenv[],
		const char token_separator[], const char token_opener[], const char token_closer[], const char do_not_escape[]);
int assemble_from_tokens_auto(char str[], SIZE_T strsize, SIZE_T tokenc, const char *tokenv[]);

#ifdef __cplusplus
}
#endif

#endif /* TOK_H_ */
