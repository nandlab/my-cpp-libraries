#ifndef TOKBASE_H_
#define TOKBASE_H_

#include "misc.h"

#if defined(__cplusplus) && ! defined(ARDUINO)
	#include <cstdio>
	#define SIZE_T std::size_t
#else
	#include <stdio.h>
	#include <stdbool.h>
	#define SIZE_T size_t
#endif

enum {
	ERROR_NONE				=	0,
	ERROR_TOO_MANY_TOKENS	=	128,
	ERROR_INVALID_QUOTE		=	129,
	ERROR_INVALID_ESCAPE	=	130,
	ERROR_ALLOC_FAILED		=	131,
	ERROR_ARR_TOO_SMALL		=	132,
};

#endif
