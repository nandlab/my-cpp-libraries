/*
 * wtok.h
 *
 *   Created on: Feb 6, 2021
 *       Author: dan
 *  Description: Library for a mixture of escape sequence decoding as in C and tokenizing as in bash
 *
 */

#ifndef WTOK_H_
#define WTOK_H_

#include "tokbase.h"

#if defined(__cplusplus) && ! defined(ARDUINO)
	#include <cwchar>
#else
	#include <wchar.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

void	w_print_with_null(wchar_t str[], SIZE_T strsize);
void	w_remove_char(wchar_t str[]);
void	w_insert_char(wchar_t str[], wchar_t c);
bool	w_needs_quoting(const wchar_t str[]);
wchar_t	w_escape_to_char(wchar_t c);
wchar_t	w_char_to_escape(wchar_t c);
int 	w_tokenize(wchar_t str[], SIZE_T *tokenc, wchar_t *tokenv[], SIZE_T max_tokens);
void	w_escape_string(wchar_t str[], const wchar_t do_not_escape[]);
void	w_assemble_from_tokens(wchar_t str[], SIZE_T tokenc, const wchar_t *tokenv[],
			const wchar_t token_separator[], const wchar_t token_opener[], const wchar_t token_closer[], const wchar_t do_not_escape[]);
int		w_assemble_from_tokens_auto(wchar_t str[], SIZE_T strsize, SIZE_T tokenc, const wchar_t *tokenv[]);

#ifdef __cplusplus
}
#endif

#endif /* WTOK_H_ */
