#ifndef LAUPBASE_H
#define LAUPBASE_H

#if defined(__cplusplus) && ! defined(ARDUINO)
	#include <cstdio>
#else
	#include <stdio.h>
#endif

enum {
	LAUP_PACKET_NOT_VALID = 90,
	LAUP_PORT = 40000,
	LAUP_ID_MAX_SIZE = 64,
};

/*
#if defined(__cplusplus)
extern "C" {
#endif

#if defined(__cplusplus)
}
#endif
*/

#endif
