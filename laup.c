#include "laup.h"
#include <stdlib.h>
#include <string.h>

#if !defined(ARDUINO)
#include "time.h"
#endif

static const char LAUP_SIGNATURE[] = "laup04";
// static const char LAUP_ACK[] = "ack";

static char* laup_generate_id(char id[], const char hostname[]) {
	static unsigned counter = 0;
	sprintf(id, "%s%03u", hostname, counter);
	counter++;
	return id;
}

/*
static void id2str(char *idstr, unsigned long id) {
	sprintf(idstr, "%lu", id);
}
*/

int laup_generate_packet(char *packet, size_t packet_size, char *id_str, const char *from, const char *to, size_t cmd_tokenc, const char *const *cmd_tokenv) {
	const size_t tokenc = cmd_tokenc+4;
	const char ** const tokenv = malloc(tokenc*sizeof(char*));
	if (tokenv == NULL) return ERROR_ALLOC_FAILED;
//	char id_str[64];
//	*id = generate_id();
//	id2str(id_str, *id);
	laup_generate_id(id_str, from);
	tokenv[0] = LAUP_SIGNATURE;
	tokenv[1] = id_str;
	tokenv[2] = from;
	tokenv[3] = to;
	for (size_t i=0; i<cmd_tokenc; i++) {
		tokenv[i+4] = cmd_tokenv[i];
	}
	int ret = assemble_from_tokens_auto(packet, packet_size, tokenc, tokenv);
	free(tokenv);
	return ret;
}

/*
int laup_generate_ack_packet(char *packet, size_t packet_size, char *id_str, const char *from, const char *to, const char *acked_id) {
	const size_t tokenc = 6;
	const char *tokenv[6];
//	char id_str[64];
//	char received_packet_id_str[64];
//	*id = generate_id();
//	id2str(id_str, *id);
//	id2str(received_packet_id_str, received_packet_id);
	laup_generate_id(id_str, from);
	tokenv[0] = LAUP_SIGNATURE;
	tokenv[1] = id_str;
	tokenv[2] = from;
	tokenv[3] = to;
	tokenv[4] = LAUP_ACK;
	tokenv[5] = acked_id;
	return assemble_from_tokens_auto(packet, packet_size, tokenc, tokenv);
}
*/

int laup_parse_packet(char *packet, char **id, char **from, char **to,
//		bool *is_ack, char *acked_id,
		size_t *cmd_tokenc, char **cmd_tokenv, size_t max_cmd_tokenc)
{
	const size_t max_tokenc = max_cmd_tokenc+4;
	size_t tokenc;
	char ** const tokenv = malloc(max_tokenc*sizeof(char*));
	if (tokenv == NULL) return ERROR_ALLOC_FAILED;
	int ret = tokenize(packet, &tokenc, tokenv, max_tokenc);
	*cmd_tokenc = tokenc-4;
	if (ret) {
		free(tokenv);
		return ret;
	}
	if (tokenc < 5 || strcmp(LAUP_SIGNATURE, tokenv[0])) {
		free(tokenv);
		return LAUP_PACKET_NOT_VALID;
	}
	*id = tokenv[1];
//	ret = sscanf(tokenv[1], "%lu", id);
//	if (ret != 1) {
//		free(tokenv);
//		return LAUP_PACKET_NOT_VALID;
//	}
	*from = tokenv[2];
	*to   = tokenv[3];
//	*is_ack = !strcmp(LAUP_ACK, tokenv[4]);
//	if (*is_ack) {
//		if (tokenc < 6) {
//			free(tokenv);
//			return LAUP_PACKET_NOT_VALID;
//		}
//		acked_id = tokenv[5];
//		ret = sscanf(tokenv[5], "%lu", acked_id);
//		if (ret != 1) {
//			free(tokenv);
//			return LAUP_PACKET_NOT_VALID;
//		}
//	}
//	else {
		for (size_t i=0; i < *cmd_tokenc; i++) {
			cmd_tokenv[i] = tokenv[i+4];
		}
//	}
	free(tokenv);
	return ERROR_NONE;
}
