#ifndef WLAUP_H_
#define WLAUP_H_

#include "wtok.h"
#include "laupbase.h"

#ifdef __cplusplus
extern "C" {
#endif

int w_laup_generate_packet(wchar_t *packet, SIZE_T packet_size, wchar_t *id, const wchar_t *from, const wchar_t *to, SIZE_T cmd_tokenc, const wchar_t *const *cmd_tokenv);
// int w_laup_generate_ack_packet(wchar_t *packet, SIZE_T packet_size, wchar_t *id, const wchar_t *from, const wchar_t *to, unsigned long received_packet_id);
int w_laup_parse_packet(wchar_t *packet, wchar_t **id, wchar_t **from, wchar_t **to,
//		bool *is_ack, unsigned long *acked_id,
		SIZE_T *cmd_tokenc, wchar_t **cmd_tokenv, SIZE_T max_cmd_tokenc);

#ifdef __cplusplus
}
#endif

#endif
