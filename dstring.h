#ifndef DSTRING_H_
#define DSTRING_H_

#include <string.h>
#include <stdlib.h>

#define DSTR_MIN_CAP 8

// A string object with dynamic memory allocation.
// Copies of the members str and cap can be used directly, but
// they will be invalidated by a method call.
// After the creation of a DString first dstr_reset() should be called for initialization.
typedef struct {
	char * str; // pointer to dynamically allocated memory
	size_t cap; // capacity (number of allocated bytes)
} DString;

// Returns the equal or next larger size to requested_cap
// from the sequence (DSTR_MIN_CAP * 2^k) with k from 0 to inf.
size_t dstr_round_cap(size_t requested_cap) {
	if (requested_cap > (SIZE_MAX / 2)) {
		return SIZE_MAX;
	}
	size_t rounded_cap = DSTR_MIN_CAP;
	while (rounded_cap < requested_cap) {
		rounded_cap *= 2;
	}
	return rounded_cap;
}

// Initializes/resets DString.
// this->cap is set to 0 and this->str to NULL.
// No memory is deallocated.
// If there was allocated memory
//   either dstr_free() should be used
//   or this->str should be saved and deallocated externally (ownership transfer),
// otherwise calling this function will result in a memory leak.
void dstr_reset(DString* this) {
	this->str = NULL;
	this->cap = 0;
}

// Reserves memory for the string by allocating/reallocating memory if necessary.
// The new this->cap will be new_cap or larger.
// If this function fails and there was memory succesfully allocated before,
// it must still be deallocated by calling dstr_free().
// Returns zero on success, non-zero on failure.
int dstr_reserve(DString* this, size_t new_cap) {
	size_t rounded_new_cap = dstr_round_cap(new_cap);
	if (rounded_new_cap == this->cap) {
		return 0;
	}
	char* new_ptr = (char*)realloc(this->str, rounded_new_cap);
	if (new_ptr == NULL) {
		return 1;
	}
	this->str = new_ptr;
	this->cap = rounded_new_cap;
	return 0;
}

// Reserves memory for the string by allocating/reallocating memory if necessary.
// new_cap should be larger than zero.
// The new this->cap will be exactly new_cap.
// If this function fails and there was memory succesfully allocated before,
// it must still be deallocated by calling dstr_free().
// Returns zero on success, non-zero on failure.
int dstr_reserve_exactly(DString* this, size_t new_cap) {
	if (new_cap == 0) {
		return 1;
	}
	if (new_cap == this->cap) {
		return 0;
	}
	char* new_ptr = (char*)realloc(this->str, new_cap);
	if (new_ptr == NULL) {
		return 1;
	}
	this->str = new_ptr;
	this->cap = new_cap;
	return 0;
}

// Deallocates memory if necessary and resets the DString.
// It is safe to call this function if the DString is resetted already,
// in this case it does nothing.
void dstr_free(DString* this) {
	if (this->str != NULL) {
		free(this->str);
	}
	dstr_reset(this);
}

#endif
